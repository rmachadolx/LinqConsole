﻿namespace LinqConsole
{
    using LivrariaLinq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    class Program
    {
        static void Main(string[] args)
        {
            List<Aluno> Alunos = ListaDeAlunos.LoadAlunos();

            Alunos = Alunos.OrderBy(X => X.Apelido).ToList();

            //  Alunos = Alunos.OrderByDescending(x => x.Apelido).ThenByDescending(x => x.DisciplinasFeitas).ToList();
            //    Alunos = Alunos.Where(x => x.DisciplinasFeitas > 10 && x.DataNascimento.Month== 3 ).ToList();


            //Alunos = Alunos.OrderBy(x => x.DisciplinasFeitas).ToList(); 

            foreach (var Aluno in Alunos)
            {

                Console.WriteLine($"Nome: {Aluno.PrimeiroNome} / " +
                    $" Apelido: {Aluno.Apelido} / " +
                    $" Data de Nascimento {Aluno.DataNascimento.ToShortDateString()} / " +
                    $"Disciplinas Feitas: {Aluno.DisciplinasFeitas} ");
            }
            int totalDisciplinasFeitas = Alunos.Sum(x => x.DisciplinasFeitas);
            Double mediaDisciplinasFeitas = Alunos.Average(x => x.DisciplinasFeitas);
           

            Console.WriteLine($"Total de disciplinas feitas: {totalDisciplinasFeitas}");
            Console.WriteLine($"Média de disciplinas feitas: {mediaDisciplinasFeitas:N2}");

            totalDisciplinasFeitas = Alunos.Where(X => X.DataNascimento.Month == 2).Sum(X => X.DisciplinasFeitas);
            mediaDisciplinasFeitas = Alunos.Where(X => X.DataNascimento.Month == 2).Average(X => X.DisciplinasFeitas);

            Console.WriteLine($"Total de disciplinas feitas: {totalDisciplinasFeitas}");
            Console.WriteLine($"Média de disciplinas feitas: {mediaDisciplinasFeitas:N2}");

            Console.ReadKey();
        }
    }
}
